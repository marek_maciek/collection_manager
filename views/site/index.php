<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Card Collection';
?>
<div class="site-index">

    <div class="jumbotron">
        <div class="col-6 mypanel">
          <h1>Welcome in card collection mannager</h1><br>
          <p><?= Html::a('Add card',['/site/add'],['class' => "btn btn-lg btn-success mybutton"])?>
            <?= Html::a('See catalog',['/site/catalog'],['class' => "btn btn-lg btn-primary mybutton"]) ?></p>
        </div>
    </div>
</div>
