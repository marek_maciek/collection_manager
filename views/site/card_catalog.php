<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div class="container mypanel">
  <?php foreach($cards as $card): ?>
      <?= Html::a(Html::img($card->file,['class'=> 'card col-md-4']),['updatecard', 'id' => $card->id]);?>
  <?php endforeach; ?>
</div>
