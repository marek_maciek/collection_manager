<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="col-md-3"></div>
  <div class="form-group mypanel-2 col-md-6">

    <?= $form->field($model,'name') ?>
    <?= $form->field($model,'cost')->textInput(['type' => 'number','value'=>3]) ?>
    <?= $form->field($model,'digivolution') ?>
    <?= $form->field($model,'power')->textInput(['type' => 'number','value'=>1000,'step' => 1000]) ?>
    <?= $form->field($model,'effect')->textArea() ?>
    <?= $form->field($model,'lv')->textInput(['type' => 'number','value'=>2]) ?>
    <?= $form->field($model,'color')->dropDownList(
            ['red'=>'Red','blue'=>'Blue']
        ); ?>
    <?= $form->field($model,'rarity') ?>
    <?= $form->field($model,'form') ?>
    <?= $form->field($model,'atribute') ?>
    <?= $form->field($model,'type') ?>
    <?= $form->field($model,'inherited')->textArea() ?>
    <?= $form->field($model,'security')->textArea() ?>
    <?= $form->field($model2,'image')->fileInput() ?>

    <?= Html::submitButton('Submit',['class' => 'btn btn-primary']) ?>
  </div>
</div>
<?php ActiveForm::end(); ?>
