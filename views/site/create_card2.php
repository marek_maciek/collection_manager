<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
<div class=" container mypanel-2 ">
<div class="col-md-1">
</div>
<div class="col-md-5">
      <?= Html::img($model->file,['class'=> 'carddisplay']); ?>
</div>
  <div class="form-group col-md-6">

    <?= $form->field($model,'name') ?>
    <?= $form->field($model,'cost')->textInput(['type' => 'number']) ?>
    <?= $form->field($model,'digivolution') ?>
    <?= $form->field($model,'power')->textInput(['type' => 'number','step' => 1000]) ?>
    <?= $form->field($model,'effect')->textArea() ?>
    <?= $form->field($model,'lv')->textInput(['type' => 'number']) ?>
    <?= $form->field($model,'color')->dropDownList(
            ['red'=>'Red','blue'=>'Blue']
        ); ?>
    <?= $form->field($model,'rarity') ?>
    <?= $form->field($model,'form') ?>
    <?= $form->field($model,'atribute') ?>
    <?= $form->field($model,'type') ?>
    <?= $form->field($model,'inherited')->textArea() ?>
    <?= $form->field($model,'security')->textArea() ?>

    <?= Html::submitButton('Submit',['class' => 'btn btn-lg btn-success mybutton']) ?>
    <?= Html::a('Delete',['/site/deletecard', 'id' => $model->id],['class' => "btn btn-lg btn-danger mybutton"]) ?>
    <?= Html::a('Back',['/site/catalog'],['class' => "btn btn-lg btn-primary mybutton"]) ?>
  </div>
</div>
</div>
<?php ActiveForm::end(); ?>
