<?php

use yii\helpers\Html;
?>
<div class="site-index">

    <div class="jumbotron">
        <div class="col-6 mypanel">
          <h1><?= $message ?></h1><br>
          <p><?= Html::a('Main menu',['/site/index'],['class' => "btn btn-lg btn-primary mybutton"])?>
            <?= Html::a('Add another',['/site/add'],['class' => "btn btn-lg btn-success mybutton"]) ?>
          <?= Html::a('To the catalog',['/site/catalog'],['class' => "btn btn-lg btn-info mybutton"]) ?></p>
        </div>
    </div>
</div>
