<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\cards;
use app\models\photo;
use yii\web\UploadedFile;

class SiteController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }


    public function actionCatalog()
    {
      $cards = cards::find()->all();
      return $this->render('card_catalog',['cards' => $cards]);
    }

    public function actionAdd()
    {
      $model = new cards();
      $model2 = new photo();

      if ($model->Load(Yii::$app->request->post()) && $model2->Load(Yii::$app->request->post()) && $model->validate())
      {
        $model2->image = UploadedFile::getInstance($model2, 'image');
        $model->file = ('../photos/' . $model2->image->baseName . '.' . $model2->image->extension);
        if($model->save() && $model2->upload())
        {
          return $this->render('create_success',['message' => 'Card Added Successfuly']);
        }
        else
        {
          print_r("not succes");
        }
      }
      else
      {
        return $this->render('create_card',['model' => $model,'model2' => $model2]);
      }
    }

    public function actionDeletecard($id)
    {
      $card = cards::find()->where(['id' => $id])->one();
      $card->delete();
      return $this->render('create_success',['message' => 'Card Deleted Successfuly']);
    }

    public function actionUpdatecard($id)
    {
      $model = new cards();

      if ($model->Load(Yii::$app->request->post()) && $model->validate())
      {
        print_r($model->cost);
        $new = cards::find()->where(['id' => $id])->one();

        $new->name = $model->name;
        $new->cost = $model->cost;
        $new->digivolution = $model->digivolution;
        $new->power = $model->power;
        $new->effect = $model->effect;
        $new->lv = $model->lv;
        $new->color = $model->color;
        $new->rarity = $model->rarity;
        $new->form = $model->form;
        $new->atribute = $model->atribute;
        $new->type = $model->type;
        $new->inherited = $model->inherited;
        $new->security = $model->security;

        $new->save();
        return $this->render('create_success',['message' => 'Card Edited Successfuly']);
      }
      else
      {
        $model = cards::find()->where(['id' => $id])->one();
        return $this->render('create_card2',['model' => $model]);
      }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
