<?php
namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class photo extends Model
{

    public $image;

    public function rules()
    {
        return [
            [['image'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }

    public function upload()
    {
        if ($this->validate())
        {
            $this->image->saveAs('../photos/' . $this->image->baseName . '.' . $this->image->extension);
            return true;
        } else {
            return false;
        }
    }
}
?>
