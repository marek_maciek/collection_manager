<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;


class cards extends ActiveRecord
{

  public function rules()
  {
    return[
      [['name','color','rarity'],'required'],
      [['cost','digivolution','power','effect','lv','form','atribute','type','inherited','security'],'safe']
    ];
  }

}
